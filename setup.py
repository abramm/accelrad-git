from distutils.core import setup
setup(name='accelrad',
      version='0.1',
      package_dir={'accelrad': 'src'},
      packages=['accelrad'],
      author='Vadym Abramchuk',
      author_email='abramm@gmail.com',
      maintainer='Vadym Abramchuk',
      maintainer_email='abramm@gmail.com',
      license='WTFPL'
      )
