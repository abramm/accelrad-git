#!/usr/bin/env python2

import re
import struct
import socket


def extract_vlan(val):
    matcher = re.compile('(vlan|VLAN|eth\d+)\.?(\d+\.\d+)')
    matched = matcher.match(radius_value(val))
    if matched is not None:
        return matched.group(2)
    else:
        return None


RADIUS_TYPE_MAP = (
    (re.compile('^"(.*)"$'), lambda m: m.group(1)),
    (re.compile('^(-?\d+)$'), lambda m: int(m.group(1))),
)


def radius_value(value_string):
    """
    Given a FreeRADIUS-provided raw value string, converts it
    to an appropriate type, using the conversion rules defined
    in RADIUS_TYPE_MAP tuple.

    >>> radius_value('"foobar"')
    'foobar'
    >>> radius_value('Wireless-802.11')
    'Wireless-802.11'
    >>> type(radius_value('"foobar"'))
    <type 'str'>
    >>> type(radius_value('-123'))
    <type 'int'>
    """
    if type(value_string) in (str, unicode):
        for regexp, processor in RADIUS_TYPE_MAP:
            m = regexp.match(value_string)
            if m is not None:
                return processor(m)
    return value_string


def ip2int(ip_address):
    return struct.unpack("!I", socket.inet_aton(ip_address))[0]


def int2ip(int_address):
    return socket.inet_ntoa(struct.pack("!I", int_address))
