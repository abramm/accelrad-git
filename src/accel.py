#! /usr/bin/env python2

import sys
import os
import MySQLdb
import threading

# rename radiusd.py as radiusd_for_test.py as it conflicts with REAL radiusd from FreeRADIUS
try:
    import radiusd
except ImportError:
    import radiusd_for_test as radiusd

import helpers


# Configuration; you really should move it elsewhere
config_db = 'python'  # Database name
config_host = 'localhost'  # Database host
config_user = 'python'  # Database user and password
config_passwd = 'python'

# xxx Database

# Globals
local_storage = threading.local()


def connect_to_database(persist=True):
    """
    Connect to database or return existing connection

    :param persist: Whether connection should be persistent or not.
    Use non-persistent connection for cases when you need another connection or when you use locking
    :rtype: MySQLdb.Connection
    """
    global local_storage
    if persist:
        try:
            db_handle = local_storage.db_handle
            db_handle.ping()
        except (AttributeError, MySQLdb.OperationalError), _:
            log(radiusd.L_DBG, "Broken or non-existing MySQL connection, creating new one")
            db_handle = make_database_connection()
            local_storage.db_handle = db_handle
        return db_handle
    else:
        return make_database_connection()


def make_database_connection():
    global config_db, config_host, config_user, config_passwd
    try:
        db_handle = MySQLdb.connect(db=config_db,
                                    host=config_host,
                                    user=config_user,
                                    passwd=config_passwd)

        log(radiusd.L_DBG, "Connected to MySQL database")
        log(radiusd.L_DBG, 'db connection: ' + str(db_handle))
        return db_handle

    except MySQLdb.OperationalError, e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        file_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log(radiusd.L_ERR, 'Caught MySQL exception %s: %s at %s:%d' % (exc_type, e, file_name, exc_tb.tb_lineno))
        return -1


def log(level, s):
    """Log function."""
    radiusd.radlog(level, s)


def instantiate(_):
    """Module Instantiation.  0 for success, -1 for failure."""
    return 0


def authorize(auth_data):
    """Authorization and authentication are done in one step."""

    db_handle = connect_to_database()

    vlan = None
    mac = None
    nas_identifier = None
    nas_ip_address = None

    for t in auth_data:
        if t[0] == 'User-Name':
            vlan = helpers.extract_vlan(t[1])
        elif t[0] == 'Calling-Station-Id':
            mac = helpers.radius_value(t[1])
        elif t[0] == 'NAS-Identifier':
            nas_identifier = helpers.radius_value(t[1])
        elif t[0] == 'NAS-IP-Address':
            nas_ip_address = helpers.radius_value(t[1])

    with db_handle as db_cursor:
        query = 'SELECT blahblahblah FROM users WHERE vlan = %s;'
        db_cursor.execute(query, vlan)
        result = db_cursor.fetchone()

        if not result:
            # This way you will send DHCP NAK to customer; you may also give customer IP from other pool or something
            return radiusd.RLM_MODULE_REJECT

        # Your logic goes here

        # Proper reply
        radius_reply_attributes = [('Framed-IP-Address', '192.168.100.40'),
                                   ('DHCP-Client-IP-Address', '192.168.100.40'),
                                   ('Framed-Netmask', '255.255.255.0'), ('DHCP-Mask', '24'),
                                   ('DHCP-Router-IP-Address', '192.168.100.40'),
                                   ('Acct-Interim-Interval', '60')]

        return (radiusd.RLM_MODULE_UPDATED,
                tuple(radius_reply_attributes),
                (('Auth-Type', 'python'),))


def authenticate(_):
    return radiusd.RLM_MODULE_OK


def preacct(_):
    return radiusd.RLM_MODULE_OK


def accounting(acct_data):
    """Accounting."""

    db_handle = connect_to_database()
    vlan = None
    acct_type = None
    acct_session_id = None
    acct_session_time = None
    (acct_input_octets, acct_output_octets, acct_input_gigawords, acct_output_gigawords) = (0, 0, 0, 0)

    for t in acct_data:
        if t[0] == 'User-Name':
            vlan = helpers.extract_vlan(t[1])
        elif t[0] == 'Acct-Status-Type':
            acct_type = helpers.radius_value(t[1])
        elif t[0] == 'Acct-Session-Id':
            acct_session_id = helpers.radius_value(t[1])
        elif t[0] == 'Acct-Session-Time':
            acct_session_time = helpers.radius_value(t[1])

        # Swap input and output
        elif t[0] == 'Acct-Input-Octets':
            acct_output_octets = helpers.radius_value(t[1])
        elif t[0] == 'Acct-Output-Octets':
            acct_input_octets = helpers.radius_value(t[1])
        elif t[0] == 'Acct-Input-Gigawords':
            acct_output_gigawords = helpers.radius_value(t[1])
        elif t[0] == 'Acct-Output-Gigawords':
            acct_input_gigawords = helpers.radius_value(t[1])

    if acct_type == 'Start':
        # Write session to database
        pass
    elif acct_type == 'Interim-Update':
        # Update session in database or return radiusd.RLM_MODULE_REJECT if there is no one.
        # This way accel-ppp will hang up unknown session.
        pass
    elif acct_type == 'Stop':
        # Remove session and log it somwehere (if you need it)
        pass

    return radiusd.RLM_MODULE_OK


def detach():
    """Detach and clean up."""
    radiusd.radlog(radiusd.L_DBG, "*** detach ***")
    return radiusd.RLM_MODULE_OK


def pre_proxy(_):
    radiusd.radlog(radiusd.L_DBG, "*** pre_proxy ***")
    return radiusd.RLM_MODULE_OK


def post_proxy(_):
    radiusd.radlog(radiusd.L_DBG, "*** post_proxy ***")
    return radiusd.RLM_MODULE_OK


def post_auth(_):
    radiusd.radlog(radiusd.L_DBG, "*** post_auth ***")
    return radiusd.RLM_MODULE_OK


def recv_coa(_):
    radiusd.radlog(radiusd.L_DBG, "*** recv_coa ***")
    return radiusd.RLM_MODULE_OK


def send_coa(_):
    radiusd.radlog(radiusd.L_DBG, "*** send_coa ***")
    return radiusd.RLM_MODULE_OK
