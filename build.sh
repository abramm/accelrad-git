#!/bin/sh

dch -v `python setup.py -V`+hg`date +%Y%m%d.%H%M%S` Rebuild
rm ../*.deb ../*.changes
dpkg-buildpackage -b -tc -uc -us
